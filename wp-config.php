<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
require __DIR__ . '/wp-content/vendor/autoload.php';


// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'silverbee' );

/** MySQL database username */
define( 'DB_USER', 'homestead' );

/** MySQL database password */
define( 'DB_PASSWORD', 'secret' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define( 'WP_CONTENT_DIR', __DIR__ . '/wp-content' );

define( 'WP_HOME', 'http://silverbee-rework.test' );

define( 'WP_SITEURL', 'http://silverbee-rework.test' );


/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ';EGTbM.a^2 :M=I_K.u5W4yp <DK$V$FCRn[le:-px<(m_Io`vF?TEg%rY/`FFfo');
define('SECURE_AUTH_KEY',  '?v1S?uI*:cYXHlqE{p{nULX(e6XmvA`yOGs3]PX 6Z-h+:VGF(oE5dk0Ez,As*6$');
define('LOGGED_IN_KEY',    ':w7@8-:UD +X+44xF^p8|?5X{3:!]1&2|]p8yC,>r ayExX)*b=|V6>VTQa}|>2i');
define('NONCE_KEY',        'YC8-j`Z,,6~t Vned=]:dyaUD!?`XDMG@|8mB7Gl@BJ6uZ2liVPr+.3(zX8W~7U2');
define('AUTH_SALT',        'Ob-8z@x)f-E_h~z]w0tt%YsdFcf| ,7^s>+.o9XWdzDj)Yt;;(,qao5kp6)-U7F?');
define('SECURE_AUTH_SALT', '?94Shnm.f.#T! i1|B@7JY!fgz-;xFWsmV3@S&f0BmXJs6TP@a-^7|eO*N+~xPSZ');
define('LOGGED_IN_SALT',   '5+?JuSXj]JQ[y_ibPVBHtq0V?u%V9)kY6lCR-tAT6y`SMC|CX6SkrM,V;f;to(l2');
define('NONCE_SALT',       '|MY^P{J0c`3lKYQbkt^UCWFR0#^[J,h$Ly2TivImZTt@57VpE<W#]>Pxo+J-9dTw');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
