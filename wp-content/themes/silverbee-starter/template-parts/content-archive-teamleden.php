<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 04/04/2019
 * Time: 13:16
 */

//$page_id = get_post( 212 );

$page_cases_id = apply_filters( 'wpml_object_id', 745, 'post', true );
$page_id = get_post($page_cases_id);

?>

<section id="section-1">
	<div class="post-content">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-11 col-md-11 col-lg-10 col-xl-8 align-self-center">
					<div class="cases-intro-container">
						<h1>
							<?php echo $page_id->post_title; ?>
						</h1>
						<p>
							<?php echo $page_id->post_content; ?>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="section-2">
	<div class="ocean-container">
		<div class="ocean">
			<div class="wave"></div>
			<div class="wave"></div>
		</div>
	</div>
</section>

<section id="section-3">
	<?php
	if ( have_posts() ) : $c = 0; ?>
		<?php
		while ( have_posts() ) : the_post(); ?>
			<div class="case-item-wrapper <?php if ( $c >= 1 ) : ?>fly-in<?php endif; ?>">
				<div class="row justify-content-center">
					<div class="col-11 col-lg-10 col-xl-8">
						<div class="container-fluid">
							<div class="row justify-content-center">
								<div class="col-12 col-sm-5 text-center col-md-5 align-self-center <?php if ( $c % 2 == 0 ) : ?>order-md-1 <?php else : ?>order-md-2 offset-lg-1 text-right<?php endif; ?>">
									<div class="case-img">
                                        <?php
                                            $teamlid_afb =  get_field('teamlid_afb');
                                            if ( !$teamlid_afb ){
                                                $teamlid_afb = get_the_post_thumbnail_url();
                                            }
                                        ?>
										<div class="page-thumb-wrapper"
										     style="background-image: url('<?php echo $teamlid_afb; ?>')">
											<svg id="overlay"
											     <?php if ( $c % 2 !== 0 ) : ?>class="overlay-white"<?php endif; ?>
											     pointer-events="none"></svg>
										</div>
									</div>
								</div>
								<div class="col-12 col-sm-7 col-md-6 <?php if ( $c % 2 == 0 ) : ?>order-md-2 offset-lg-1 <?php else : ?>order-md-1<?php endif; ?> align-self-center">
									<div class="case-desc">
										<h3><?php echo get_the_title(); ?></h3>
										<p>
											<?php echo get_the_excerpt(); ?>
										</p>
										<div class="btn-container">
											<a href="<?php echo get_the_permalink(); ?>" class="btn-primary">
                                                <span>
                                                    <?php echo __('Lees meer over mij', 'silverbee-starter'); ?>
                                                </span>
											</a>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php $c ++; endwhile;
	endif; ?>
</section>