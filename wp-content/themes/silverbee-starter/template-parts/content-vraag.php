<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 04/04/2019
 * Time: 16:44
 */

if ( is_tax() ) {
	$term_id = get_queried_object();
	$posts   = get_field( 'page_gekoppeld_teamlid', $term_id );
} elseif ( is_archive() ) {
	$term_id = apply_filters( 'wpml_object_id', 212, 'post', true );
	$posts = get_field( 'page_gekoppeld_teamlid', $term_id );
} elseif ( is_singular( 'diensten' ) ) {
	$term_id = get_the_id();
	$posts   = get_field( 'dienst_gekoppeld_teamlid', $term_id );
} elseif ( is_singular( 'cases' ) ) {
	$term_id = get_the_id();
	$posts   = get_field( 'case_gekoppeld_teamlid', $term_id );
} elseif ( is_home() ) {
	$term_id = apply_filters( 'wpml_object_id', 64, 'post', true );
	$posts   = get_field( 'page_gekoppeld_teamlid', $term_id );
} elseif ( is_singular( 'post' ) ) {
	$term_id = get_the_id();
	$posts   = get_field( 'blog_gekoppeld_teamlid', $term_id );
} else {
    $id = get_the_id();
	$posts = get_field( 'page_gekoppeld_teamlid', $id );
}

?>

<section id="vraag-wrapper">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-11 col-lg-10 col-xl-8">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-xl-5 align-self-center">
                            <div class="vraag-content">
                                <h3>
	                                <?php echo __('Heeft u een vraag?', 'silverbee-starter'); ?>
                                </h3>
                                <p>
									<?php
									if ( $posts ): ?>
										<?php foreach ( $posts as $post ) :
											setup_postdata( $post );
											echo get_the_excerpt();
										endforeach;
									endif;
									?>
                                </p>
                                <div class="btn-container">
                                    <a data-toggle="modal" data-target="#employeeContact" class="btn-primary">
                                        <span>
                                            <?php echo __('Neem contact met mij op', 'silverbee-starter'); ?>
                                        </span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-sm-6 col-lg-6 offset-xl-1 col-xl-6">
                            <div class="teamlid-wrapper">
								<?php
								if ( $posts ): ?>
									<?php foreach ( $posts as $post ) :
										setup_postdata( $post );
										$teamlid_id       = get_the_ID( $post );
										$teamlid_function = get_field( 'teamlid_function' );
										$teamlid_mail     = get_field( 'teamlid_mail' );
										$teamlid_telefoon = get_field( 'teamlid_telefoon' );
										$teamlid_linkedin = get_field( 'teamlid_linkedin' );
										?>
                                        <div class="teamlid-thumbnail">
                                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                                            <div class="teamlid-desc">
                                                <div class="content">
                                                    <span><?php the_title(); ?></span>
                                                    <span><?php echo get_field( 'teamlid_function' ); ?></span>
                                                    <div class="social">
														<?php
														if ( $teamlid_linkedin ) : ?>
                                                            <span>
                                                                <a href="<?php echo $teamlid_linkedin; ?>"
                                                                   target="_blank">
                                                                    <?php get_template_part( 'img/in.svg' ); ?>
                                                                </a>
                                                            </span>
														<?php endif ?>
														<?php
														if ( $teamlid_mail ) : ?>
                                                            <span>
                                                                <a href="mailto:<?php echo $teamlid_mail; ?>"
                                                                   target="_blank">
                                                                    <?php get_template_part( 'img/mail.svg' ); ?>
                                                                </a>
                                                            </span>
														<?php endif ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal fade" id="employeeContact" tabindex="-1" role="dialog"
                                             aria-labelledby="employeeContact" aria-hidden="true">
                                            <div class="modal-dialog modal-dialog-centered" role="document">
                                                <div class="modal-content mc-cf-7">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    <div class="modal-header">
                                                        <h5 class="modal-title"
                                                            id="exampleModalLabel">
															<?php echo get_the_title(); ?></h5>
                                                    </div>
                                                    <div class="function">
														<?php echo $teamlid_function; ?>
                                                    </div>
                                                    <div class="extra-teamlid">
														<?php if ( $teamlid_mail ) : ?>
                                                            <div class="left">
                                                                <img src="/wp-content/themes/silverbee-starter/img/mail.png"
                                                                     alt="">
                                                                <a href="mailto:<?php echo get_field( 'teamlid_mail' ); ?>">
                                                                    <span class="mail"><?php echo get_field( 'teamlid_mail' ); ?></span>
                                                                </a>
                                                            </div>
														<?php endif; ?>
														<?php if ( $teamlid_telefoon ) : ?>
                                                            <div class="right">
                                                                <img src="/wp-content/themes/silverbee-starter/img/phone.png"
                                                                     alt="">
                                                                <a href="tel:<?php echo get_field( 'teamlid_telefoon' ); ?>">
                                                                    <span class="phone"><?php echo get_field( 'teamlid_telefoon' ); ?></span>
                                                                </a>
                                                            </div>
														<?php endif; ?>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="contact-form-7">
															<?php
															$contact_form = get_field( 'contact_form', $teamlid_id );
															$cf_ID        = $contact_form->ID;
															$cf_title     = $contact_form->post_title;


															$contact_form_sc = "[contact-form-7 id='" . $cf_ID . " title='" . $cf_title . "']";

															echo do_shortcode( $contact_form_sc );

															?>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
									<?php endforeach;
									wp_reset_postdata();
								endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
