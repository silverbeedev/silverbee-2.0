<?php
/**
 * Created by PhpStorm.
 * User: tomaszwilczek
 * Date: 19/02/2019
 * Time: 09:00
 */

?>

<section id="contact-banner">
    <div class="contact-banner_image-container">
        <span>
            <?php echo __('Benieuwd naar wat wij voor jou kunnen betekenen?', 'silverbee-starter'); ?>
        </span>
        <div class="btn-container">
            <?php $c_id = apply_filters( 'wpml_object_id', 123, 'post', true ); ?>
            <a href="<?php echo get_permalink($c_id); ?>" class="btn-outline">
	            <?php echo __('Neem contact met ons op', 'silverbee-starter'); ?>
            </a>
        </div>
    </div>
</section>
