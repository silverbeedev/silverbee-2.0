<?php
/**
 * Created by PhpStorm.
 * User: tomaszwilczek
 * Date: 13/02/2019
 * Time: 10:27
 */

?>
<section id="section-1">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-10 col-11">
                <div class="front-cta-container">
                    <h1>
                        <?php

                        $words    = explode( ' ', get_the_content( '', '',  false ) );
                        //$words[0] = '<span>' . $words[0] . '</span>';
                        foreach ($words as $word) {
                            echo '<span> ' . $word . ' </span>';
                        }
                        ?>
                    </h1>
                    <div class="btn-container">
                        <a href="<?php echo get_post_type_archive_link('cases'); ?>" class="btn-primary">
                            <span>
                                <?php echo __('Zie succesverhalen', 'silverbee-starter'); ?>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="image-container">
        <div id="movable-image" class="image"
             style="background-image: url('<?php echo get_the_post_thumbnail_url() ?>')"></div>
        <a  href="tel:+31 (0) 40 751 03 35" class="play-container">
            <div class="icon-container">

                <div class="icon">
                    <?php get_template_part('img/phone.svg'); ?>
                </div>
            </div>
        </a>
        <div class="bg-play">

        </div>

    </div>
</section>
