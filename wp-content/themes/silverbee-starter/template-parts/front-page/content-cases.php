<?php

$args = array(
	'posts_per_page'   => 4,
	'orderby'          => 'date',
	'order'            => 'ASC',
	'post_type'        => 'cases',
	'post_status'      => 'publish',
	'suppress_filters' => false
);

$cases = get_posts( $args );

?>

<section id="section-2">
    <div class="ocean-container">
        <div class="ocean">
            <div class="wave"></div>
            <div class="wave"></div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row justify-content-sm-center justify-content-md-start">
            <div class="col-xl-7 col-lg-7 col-md-6">
                <div class="svg-container">

                    <img class="svg-mask" src="/wp-content/themes/silverbee-starter/dist/img/hex.png">
                    <img class="svg-mask" src="/wp-content/themes/silverbee-starter/dist/img/hex.png">
                    <img class="svg-mask" src="/wp-content/themes/silverbee-starter/dist/img/hex.png">
                    <img class="svg-mask" src="/wp-content/themes/silverbee-starter/dist/img/hex.png">
					<?php
					$i            = 0;
					foreach ( $cases as $case ) :
						$case_id = $case->ID;
						$case_hex = get_field( 'case_honinggraad_image', $case_id );
						?>

						<?php if ( $i === 2 ) : ?>
                        <img class="svg-mask" src="/wp-content/themes/silverbee-starter/dist/img/hex.png">
					<?php endif; ?>
                        <img class="svg-mask" src="<?php echo $case_hex; ?>">

						<?php $i ++; endforeach; ?>

                    <img class="svg-mask" src="/wp-content/themes/silverbee-starter/dist/img/hex.png">
                    <img class="svg-mask" src="/wp-content/themes/silverbee-starter/dist/img/hex.png">
                </div>

            </div>
            <div class="col-xl-3 col-lg-4 col-md-5 col-sm-11 col-12">
                <div class="cases-intro-container">
					<?php
					$case_id = apply_filters( 'wpml_object_id', 212, 'post', true );
					$post    = get_post( $case_id );
					echo $content;
					?>
                    <h2>
						<?php echo get_the_title( $case_id ); ?>
                    </h2>
                    <p>
						<?php echo get_the_excerpt( $case_id ); ?>
                    </p>
                    <div class="btn-container">
                        <a href="<?php echo get_post_type_archive_link( 'cases' ); ?>" class="btn-primary">
                            <span>
                                <?php echo __( 'Zie succesverhalen', 'silverbee-starter' ); ?>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>