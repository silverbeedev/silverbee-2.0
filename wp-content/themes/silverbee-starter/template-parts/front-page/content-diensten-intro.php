<?php

$taxonomy = get_terms( 'hoofddiensten', array( 'hide_empty' => 0 ));
$i = 3;

$bootstrap_col_left = 'order-2 order-md-1 col-11 col-md-6 col-xl-4 col-lg-5 sm-order-2 align-self-center';
$bootstrap_col_right = 'order-2 order-md-2 col-11 col-md-6 col-xl-4 offset-xl-1 col-lg-5 offset-lg-1 align-self-center';

$bootstrap_col_left_img = 'order-1 order-md-2 col-6 col-md-5 col-xl-3 offset-xl-1 col-lg-4 offset-lg-1  align-self-center';
$bootstrap_col_right_img = 'order-1 order-md-1 col-6 col-md-5 col-xl-3 col-lg-4 align-self-center';

?>

<div class="front-diensten">
    <?php foreach ($taxonomy as $hoofddienst) :
            $image_path = get_field('hoofddienst_image', $hoofddienst);
	        $dienst_excerpt = get_field('hoofddienst_excerpt', $hoofddienst);
        ?>
        <section id="section-<?php echo $i; ?>" class="<?php echo $image_path; ?>">
            <div class="container-fluid">
                <div class="row justify-content-center h-100">
                    <div class="<?php if ($i % 2 !== 0): echo $bootstrap_col_left;?><?php else : echo $bootstrap_col_right; endif ?>">
                        <div class="cases-intro-container fly-in">
                            <h2>
                                <?php echo $hoofddienst->name; ?>
                            </h2>
                            <p>
	                            <?php echo $dienst_excerpt; ?>
                            </p>
                            <div class="btn-container">
                                <?php
                                $page_diensten_id = apply_filters( 'wpml_object_id', 239, 'post', true );
                                $page_dienst = get_post($page_diensten_id);
                                $page_name = $page_dienst->post_name;
                                ?>
                                <a href="<?php echo get_home_url().'/'.$page_name.'/'.$hoofddienst->slug ?>" class="btn-primary">
                                    <span>
                                        <?php echo __('Lees meer', 'silverbee-starter'); ?>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="<?php if ($i % 2 !== 0): echo $bootstrap_col_left_img; ?><?php else : echo $bootstrap_col_right_img; endif; ?>">
                        <div class="graphic fly-in"><?php get_template_part('img/'.$image_path.'.svg'); ?></div>
                    </div>
                </div>
            </div>
        </section>

    <?php
    $i++;
    endforeach; ?>
</div>