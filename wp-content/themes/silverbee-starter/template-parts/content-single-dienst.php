<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 04/04/2019
 * Time: 13:16
 */

?>

<section id="section-1" class="dienst-single">
    <div class="post-content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-lg-11 col-xl-11">
                    <div class="container-fluid">
                        <div class="row justify-content-center h-100">
                            <div class="col-sm-11 col-md-6 order-2 order-md-1 col-xl-5 col-lg-5 align-self-center">
                                <div class="cases-intro-container">
                                    <h1>
										<?php echo get_the_title(); ?>
                                    </h1>
									<?php
									while ( have_posts() ) : the_post();
										the_content();
									endwhile; // End of the loop.
									?>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md-5 order-1 order-md-2 col-xl-3 offset-xl-1 col-lg-5 offset-lg-1 align-self-center">
                                <div class="page-thumb-wrapper hex-bg">
                                    <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="section-2">
    <div class="ocean-container">
        <div class="ocean">
            <div class="wave"></div>
            <div class="wave"></div>
        </div>
    </div>
	<?php get_template_part('template-parts/content', 'related-case') ?>
</section>