<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 06/03/2019
 * Time: 14:19
 */

?>

<div class="container-fluid">
    <div class="row justify-content-center">
		<?php if ( have_rows( 'page_content_blokken' ) ):
			while ( have_rows( 'page_content_blokken' ) ): the_row(); ?>
				<?php
				$blok_links  = get_sub_field( 'blok_links' );
				$blok_rechts = get_sub_field( 'blok_rechts' );
				?>
                <div class="<?php if ($blok_rechts) { echo 'col-11 col-lg-5 col-xl-4'; } else { echo 'col-11 col-lg-10 col-xl-8'; } ?>">
					<?php echo $blok_links; ?>
                </div>

				<?php if ( $blok_rechts ) : ?>
                    <div class="col-11 col-lg-5 col-xl-4">
						<?php echo $blok_rechts; ?>
                    </div>
				<?php
				endif;
			endwhile;
		endif; ?>
    </div>
</div>
