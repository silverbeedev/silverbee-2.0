<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 04/04/2019
 * Time: 13:16
 */

?>

<section id="section-1" class="case-single">
    <div class="post-content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-lg-11 col-xl-11">
                    <div class="container-fluid">
                        <div class="row justify-content-center h-100">
                            <div class="col-md-6 order-2 order-md-1 col-xl-5 col-lg-5 align-self-center">
                                <div class="cases-intro-container">
                                    <h1>
										<?php echo get_the_title(); ?>
                                    </h1>
									<?php
									while ( have_posts() ) : the_post();
										the_content();
									endwhile; // End of the loop.
									?>
									<?php $case_website = get_field( 'case_website' );
									if ( $case_website ) :
										?>
                                        <div class="btn-container">
                                            <a href="<?php echo $case_website ?>" class="btn-primary" target="_blank">
                                                <span> <?php echo __('Bezoek', 'silverbee-starter').' '.get_the_title(); ?></span>
                                            </a>
                                        </div>
									<?php endif; ?>

                                </div>
                            </div>
                            <div class="col-md-5 order-1 order-md-2 col-xl-3 offset-xl-1 col-lg-5 offset-lg-1 align-self-center">
                                <div class="page-thumb-wrapper"
                                     style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">
                                    <svg id="overlay" pointer-events="none"></svg>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="section-2">
    <div class="ocean-container">
        <div class="ocean">
            <div class="wave"></div>
            <div class="wave"></div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-12 col-lg-11 col-xl-11">
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-11 col-lg-11 col-xl-9">
                            <h3>
                                <?php
                                    echo __('Bijbehorende diensten', 'silverbee-starter');
                                ?>
                            </h3>
                            <div class="tax-diensten-wrapper">
                                <div class="row">
									<?php
									$posts = get_field( 'case_gekoppelde_diensten' );
									if ( $posts ): ?>
										<?php foreach ( $posts as $post ) :
											setup_postdata( $post );

											$terms = get_the_terms($id, 'hoofddiensten');

											$diensten = 'diensten';
											if (ICL_LANGUAGE_CODE=='en'){
												$diensten = 'services';
											}
                                            elseif (ICL_LANGUAGE_CODE=='es'){
												$diensten = 'servicios';
											}

											foreach ($terms as $term){
												$term_name = $term->name;
												$term_slug = $term->slug;
											}

											foreach ($terms as $term){
												$term_name = $term->name;
												$term_slug = $term->slug;
											}

											$dienst_icon  = get_the_post_thumbnail_url();
											$dienst_title = get_the_title();
											$dienst_desc  = get_the_excerpt();
											$dienst_link  = get_the_permalink();

											?>

                                            <div class="col-12 col-sm-6 col-md-6 col-lg-4 margin dienst-card">
                                                <article class="tax-dienst fly-in">
                                                    <div class="dienst-img">
                                                        <img src="<?php echo $dienst_icon; ?>" alt="">
                                                    </div>
                                                    <div class="dienst-content">
                                                        <div class="dienst-title">
                                                            <h4>
																<?php echo $dienst_title; ?>
                                                            </h4>
                                                        </div>
                                                        <div class="dienst-desc">
															<?php echo $dienst_desc; ?>
                                                        </div>
                                                    </div>
                                                    <div class="dienst-footer">
                                                        <div class="dienst-hfd">
                                                            <a href="<?php echo get_home_url().'/'.$diensten.'/'.$term_slug; ?>" class="unset">
		                                                        <?php echo $term_name; ?>
                                                            </a>
                                                        </div>
                                                        <div class="read-more-btn">
                                                            <a href="<?php echo $dienst_link; ?>" class="invert">
                                                                <?php echo __('Lees meer', 'silverbee-starter'); ?>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>

										<?php endforeach;
										wp_reset_postdata();
									endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>