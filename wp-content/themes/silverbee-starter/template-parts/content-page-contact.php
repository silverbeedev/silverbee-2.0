<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 06/03/2019
 * Time: 14:51
 */ ?>

<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-sm-10 order-2 col-md-6 order-md-1 col-lg-5">
            <div class="teamlid-wrapper">
				<?php
				$posts = get_field( 'page_gekoppeld_teamlid' );
				if ( $posts ): ?>
					<?php foreach ( $posts as $post ) :
						setup_postdata( $post ); ?>
                        <div class="teamlid-thumbnail mirror">
                            <img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
                            <div class="teamlid-desc">
                                <div class="content">
                                    <span><?php the_title(); ?></span>
                                    <span><?php echo get_field( 'teamlid_function' ); ?></span>
                                    <div class="social">
                                            <span>
                                                <a href="<?php echo get_field( 'teamlid_linkedin' ); ?>"
                                                   target="_blank">
                                                    <?php get_template_part( 'img/in.svg' ); ?>
                                                </a>
                                            </span>
                                        <span>
                                                <a href="mailto:<?php echo get_field( 'teamlid_mail' ); ?>"
                                                   target="_blank">
                                                    <?php get_template_part( 'img/mail.svg' ); ?>
                                                </a>
                                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>
					<?php endforeach;
					wp_reset_postdata();
				endif; ?>
            </div>
        </div>
        <div class="col-sm-11 order-1 col-md-5 order-md-2 col-lg-5 align-self-center">
            <div class="form-container fly-in">
				<?php
				$posts = get_field( 'page_gekoppeld_teamlid' );
				if ( $posts ):
					foreach ( $posts as $post ) :
						setup_postdata( $post );
						$contact_form = get_field( 'contact_form' );
						$cf_ID    = $contact_form->ID;
						$cf_title = $contact_form->post_title;


						$contact_form_sc = "[contact-form-7 id='" . $cf_ID . " title='" . $cf_title . "']";

						echo do_shortcode( $contact_form_sc );
					endforeach;
				endif;

				?>
            </div>
        </div>
    </div>
</div>
