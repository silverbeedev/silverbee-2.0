<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 04/04/2019
 * Time: 13:16
 */

$page_id = apply_filters( 'wpml_object_id', 64, 'post', true );

?>

<section id="section-1">
	<div class="post-content">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-sm-12 col-lg-10 col-xl-8">
					<div class="container-fluid">
						<div class="row h-100">
							<div class="col-md-8 col-xl-8 col-lg-8 align-self-center">
								<div class="cases-intro-container">
									<h1>
										<?php echo get_the_title($page_id); ?>
									</h1>
									<p>
										<?php echo get_the_excerpt($page_id); ?>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="section-2">
	<div class="ocean-container">
		<div class="ocean">
			<div class="wave"></div>
			<div class="wave"></div>
		</div>
	</div>
	<div class="container-fluid">
		<?php get_template_part('template-parts/content', 'blog-archive') ?>
	</div>
</section>

