<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 04/04/2019
 * Time: 13:16
 */

?>

<section id="section-1" class="blog-single">
	<div class="post-content">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-sm-12 col-xl-11">
					<div class="container-fluid">
						<div class="row justify-content-center h-100">
							<div class="col-md-6 col-xl-5 col-lg-5 align-self-center">
								<div class="cases-intro-container">
                                    <span><?php echo get_the_date(); ?></span>
									<h2>
										<?php echo get_the_title(); ?>
									</h2>
                                    <?php echo get_field('blog_inleiding'); ?>
								</div>
							</div>
							<div class="col-md-6 col-xl-3 offset-xl-1 col-lg-5 offset-lg-1 align-self-center">
								<div class="page-thumb-wrapper"
								     style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">
									<svg id="overlay" pointer-events="none"></svg>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="section-2" class="blog-content">
	<div class="ocean-container">
		<div class="ocean">
			<div class="wave"></div>
			<div class="wave"></div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-sm-12 col-xl-11">
				<div class="container-fluid">
					<div class="row justify-content-center">
						<div class="col-12 col-lg-11 col-xl-9">
							<div class="entry-content">
								<?php
								while ( have_posts() ) : the_post();
									the_content();
								endwhile; // End of the loop.
								?>
                            </div>
                            <div class="blog-post_footer">
                                <p class="written-by">
                                    <?php echo __('Geschreven door:', 'silverbee-starter'); ?>
                                </p>
                                    <span class="author_image">
                                        <?php $author_id = get_the_author_meta( 'ID' ); ?>
                                        <img src="<?php echo get_field( 'user_avatar', 'user_' . $author_id ); ?>" alt="">
                                    </span>
                                <span class="author_name"><?php the_author(); ?></span>
                            </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>