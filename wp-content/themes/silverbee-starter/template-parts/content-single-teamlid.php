<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 04/04/2019
 * Time: 13:16
 */

$teamlid_id       = get_the_id();
$teamlid_function = get_field( 'teamlid_function' );
$teamlid_mail     = get_field( 'teamlid_mail' );
$teamlid_telefoon = get_field( 'teamlid_telefoon' );
$teamlid_linkedin = get_field( 'teamlid_linkedin' );

?>
<section id="section-1" class="case-single teamlid-single">
	<div class="post-content">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-12 col-sm-12 col-lg-11 col-xl-11">
					<div class="container-fluid">
						<div class="row justify-content-center h-100">
							<div class="col-md-6 order-2 order-md-1 col-xl-5 col-lg-5 align-self-center">
								<div class="cases-intro-container">
									<h1>
										<?php echo get_the_title(); ?>
									</h1>
                                    <div class="function">
                                        <?php echo $teamlid_function; ?>
                                    </div>
									<?php
									while ( have_posts() ) : the_post();
										the_content();
									endwhile; // End of the loop.
									?>
                                    <div class="btn-container">
                                        <a data-toggle="modal" data-target="#employeeContact" class="btn-primary">
                                        <span>
                                            <?php echo __('Neem contact met mij op', 'silverbee-starter'); ?>
                                        </span>
                                        </a>
                                    </div>
								</div>
							</div>
							<div class="col-md-5 order-1 order-md-2 col-xl-3 offset-xl-1 col-lg-5 offset-lg-1 align-self-center">
								<?php
								$teamlid_afb =  get_field('teamlid_afb');
								if ( !$teamlid_afb ){
									$teamlid_afb = get_the_post_thumbnail_url();
								}
								?>
								<div class="page-thumb-wrapper"
								     style="background-image: url('<?php echo $teamlid_afb; ?>')">
									<svg id="overlay" pointer-events="none"></svg>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="section-2">
	<div class="ocean-container">
		<div class="ocean">
			<div class="wave"></div>
			<div class="wave"></div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-12 col-sm-12 col-md-12 col-lg-11 col-xl-11">
				<div class="container-fluid">
					<div class="row justify-content-center">
						<div class="col-12 col-md-11 col-lg-11 col-xl-9">
							<h3>
								<?php echo __('Expertises van', 'silverbee-starter').' '.get_the_title(); ?>
							</h3>
							<div class="tax-diensten-wrapper">
								<div class="row">
									<?php
									$posts = get_field( 'teamlid_gekoppelde_diensten' );
									if ( $posts ): ?>
										<?php foreach ( $posts as $post ) :
											setup_postdata( $post );
											$id = get_the_id();
											$terms = get_the_terms($id, 'hoofddiensten');

											$diensten = 'diensten';
										    if (ICL_LANGUAGE_CODE=='en'){
											    $diensten = 'services';
                                            }
                                            elseif (ICL_LANGUAGE_CODE=='es'){
	                                            $diensten = 'servicios';
                                            }

											foreach ($terms as $term){
											    $term_name = $term->name;
											    $term_slug = $term->slug;
                                            }

											$dienst_icon  = get_the_post_thumbnail_url();
											$dienst_title = get_the_title();
											$dienst_desc  = get_the_excerpt();
											$dienst_link  = get_the_permalink();
											?>
											<div class="col-12 col-sm-6 col-md-6 col-lg-4 dienst-card margin">
												<article class="tax-dienst fly-in">
													<div class="dienst-img">
														<img src="<?php echo $dienst_icon; ?>" alt="">
													</div>
													<div class="dienst-content">
														<div class="dienst-title">
															<h4>
																<?php echo $dienst_title; ?>
															</h4>
														</div>
														<div class="dienst-desc">
															<?php echo $dienst_desc; ?>
														</div>
													</div>
													<div class="dienst-footer">
														<div class="dienst-hfd">
                                                            <a href="<?php echo get_home_url().'/'.$diensten.'/'.$term_slug; ?>" class="unset">
	                                                            <?php echo $term_name; ?>
                                                            </a>
														</div>
														<div class="read-more-btn">
															<a href="<?php echo $dienst_link; ?>" class="invert">
																<?php echo __('Lees meer', 'silverbee-starter'); ?>
															</a>
														</div>
													</div>
												</article>
											</div>

										<?php endforeach;
										wp_reset_postdata();
									endif; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="modal fade" id="employeeContact" tabindex="-1" role="dialog"
     aria-labelledby="employeeContact" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content mc-cf-7">
            <button type="button" class="close" data-dismiss="modal"
                    aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class="modal-header">
                <h5 class="modal-title"
                    id="exampleModalLabel">
					<?php echo get_the_title(); ?></h5>
            </div>
            <div class="function">
				<?php echo $teamlid_function; ?>
            </div>
            <div class="extra-teamlid">
				<?php if ( $teamlid_mail ) : ?>
                    <div class="left">
                        <img src="/wp-content/themes/silverbee-starter/img/mail.png"
                             alt="">
                        <a href="mailto:<?php echo get_field( 'teamlid_mail' ); ?>">
                            <span class="mail"><?php echo get_field( 'teamlid_mail' ); ?></span>
                        </a>
                    </div>
				<?php endif; ?>
				<?php if ( $teamlid_telefoon ) : ?>
                    <div class="right">
                        <img src="/wp-content/themes/silverbee-starter/img/phone.png"
                             alt="">
                        <a href="tel:<?php echo get_field( 'teamlid_telefoon' ); ?>">
                            <span class="phone"><?php echo get_field( 'teamlid_telefoon' ); ?></span>
                        </a>
                    </div>
				<?php endif; ?>
            </div>
            <div class="modal-body">
                <div class="contact-form-7">
					<?php
					$contact_form = get_field( 'contact_form', $teamlid_id );
					$cf_ID        = $contact_form->ID;
					$cf_title     = $contact_form->post_title;


					$contact_form_sc = "[contact-form-7 id='" . $cf_ID . " title='" . $cf_title . "']";

					echo do_shortcode( $contact_form_sc );

					?>
                </div>

            </div>
        </div>
    </div>
</div>