<?php
/**
 * Created by PhpStorm.
 * User: tomaszwilczek
 * Date: 19/02/2019
 * Time: 09:00
 */

?>
<section id="blog-archive">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-xl-8 col-12">
                <h2 class="text-center">Blog</h2>
                <div class="masonry-row">
					<?php
					if ( is_home() ) {
						$the_query = new WP_Query( 'posts_per_page=-1' );
					} else {
						$the_query = new WP_Query( 'posts_per_page=5' );
					}
					?>
					<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="blog-post-container fly-in">
                            <div class="blog-post">
                                <a href="<?php echo get_the_permalink() ?>">
                                    <div class="blog-post_image-container"
                                         style="background-image: url('<?php the_post_thumbnail_url() ?>')">

                                    </div>
                                </a>
                                <div class="blog-post_header">
                                    <span class="date">
                                        <?php the_date(); ?>
                                    </span>
                                    <span class="title">
                                        <a href="<?php echo get_the_permalink() ?>">
                                            <?php the_title(); ?>
                                        </a>
                                    </span>
                                </div>
                                <div class="blog-post_content">
									<?php the_excerpt(); ?>
                                </div>
                                <div class="blog-post_footer">
                                    <span class="author_image">
                                        <?php $author_id = get_the_author_meta( 'ID' ); ?>
                                        <img src="<?php echo get_field( 'user_avatar', 'user_' . $author_id ); ?>"
                                             alt="">
                                    </span>
                                    <span class="author_name"><?php the_author(); ?></span>
                                    <a href="<?php echo get_the_permalink() ?>" class="btn-blog">
										<?php echo __( 'Lees meer', 'silverbee-starter' ); ?>
                                    </a>
                                </div>
                            </div>
                        </div>
					<?php
					endwhile;
					wp_reset_postdata();
					?>

                </div>
            </div>
        </div>
    </div>
</section>