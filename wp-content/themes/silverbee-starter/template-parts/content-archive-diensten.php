<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 04/04/2019
 * Time: 13:16
 */

$page_id = get_post( 239 );

?>

<section id="section-1">
    <div class="post-content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-11 col-md-11 col-lg-10 col-xl-8 align-self-center">
                    <div class="cases-intro-container">
                        <h1>
							<?php echo $page_id->post_title; ?>
                        </h1>
                        <p>
							<?php echo $page_id->post_content; ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="section-2">
    <div class="ocean-container">
        <div class="ocean">
            <div class="wave"></div>
            <div class="wave"></div>
        </div>
    </div>
    <div class="archive-diensten">
		<?php get_template_part( 'template-parts/front-page/content', 'diensten-intro' ) ?>
    </div>
</section>