<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 04/04/2019
 * Time: 16:44
 */ ?>

<section id="related-case-wrapper">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-11 col-lg-10 col-xl-8">
				<div class="row">
					<div class="col-12 col-sm-5 text-center col-md-5 align-self-center">
						<div class="case-img">
							<?php
							if ( is_tax() ) {
								$term_id = get_queried_object();
								$posts = get_field( 'page_gekoppelde_case', $term_id );
							}
							elseif ( is_single() ){
							    $term_id = get_the_id();
								$posts = get_field( 'dienst_gekoppelde_cases', $term_id );
                            }
							else{
								$posts = get_field( 'page_gekoppelde_case' );
							}
							if ( $posts ): ?>
								<?php foreach ( $posts as $post ) :
									setup_postdata( $post ); ?>
									<div class="page-thumb-wrapper" style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">
										<svg id="overlay" pointer-events="none"></svg>
									</div>

								<?php endforeach;
								wp_reset_postdata();
							endif; ?>
						</div>
					</div>
					<div class="col-12 col-sm-7 col-md-6 offset-md-1 align-self-center">
						<div class="case-desc">
							<h5>
								<?php echo __('Gerelateerde case', 'silverbee-starter'); ?>
                            </h5>
							<?php
							if ( is_tax() ) {
								$term_id = get_queried_object();
								$posts = get_field( 'page_gekoppelde_case', $term_id );
							}
                            elseif ( is_singular('diensten') ){
								$posts = get_field( 'dienst_gekoppelde_cases' );
							}
							else{
								$posts = get_field( 'page_gekoppelde_case' );
							}
							if ( $posts ): ?>
								<?php foreach ( $posts as $post ) :
									setup_postdata( $post ); ?>
									<h3><?php echo get_the_title(); ?></h3>
									<p>
										<?php echo get_the_excerpt(); ?>
									</p>
									<div class="btn-container">
										<a href="<?php echo get_the_permalink(); ?>" class="btn-primary">
											<span>
                                                <?php echo __('Bekijk succesverhaal', 'silverbee-starter'); ?>
                                            </span>
										</a>
									</div>
								<?php endforeach;
								wp_reset_postdata();
							endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
