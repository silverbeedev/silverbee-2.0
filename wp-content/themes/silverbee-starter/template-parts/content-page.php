<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Silverbee_Starter
 */

?>

<div class="post-content">
    <div class="container-fluid">
        <div class="row justify-content-center h-100">
            <div class="order-2 order-md-1 col-11 col-md-6 col-xl-4 col-lg-5 align-self-center">
                <h1>
					<?php echo get_the_title(); ?>
                </h1>
                <p>
					<?php echo get_field( 'page_inleiding' ); ?>
                </p>
            </div>

            <div class="order-1 order-md-2 col-md-5 col-xl-3 offset-xl-1 col-lg-5 align-self-center">
                <div class="page-thumb-wrapper"
                     style="background-image: url('<?php echo get_the_post_thumbnail_url(); ?>')">
                    <svg id="overlay" pointer-events="none"></svg>
                </div>
            </div>
        </div>
    </div>
</div>
