<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 04/04/2019
 * Time: 13:16
 */

$term_id = get_queried_object();

$tax_name = $term_id->name;
$tax_desc = $term_id->description;
$tax_img  = get_field( 'hoofddienst_image', $term_id );

?>

<section id="section-1">
    <div class="post-content">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-sm-12 col-md-11 col-xl-11">
                    <div class="container-fluid">
                        <div class="row justify-content-center h-100">
                            <div class="order-2 order-sm-1 col-sm-7 col-md-6 col-xl-4 col-lg-5 align-self-center">
                                <div class="cases-intro-container">
                                    <h2>
										<?php echo $tax_name; ?>
                                    </h2>
                                    <p>
										<?php echo $tax_desc; ?>
                                    </p>
                                </div>
                            </div>
                            <div class="col-8 text-center order-1 order-sm-2 col-sm-5 col-md-6 col-xl-4 offset-xl-1 col-lg-5 offset-lg-1 align-self-center">
                                <div class="graphic <?php echo $tax_img; ?>"><?php get_template_part( 'img/' . $tax_img . '.svg' ); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="section-2">
    <div class="ocean-container">
        <div class="ocean">
            <div class="wave"></div>
            <div class="wave"></div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-12 col-lg-11 col-xl-11">
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-12 col-md-11 col-lg-11 col-xl-9">
                            <h3>
                                <?php echo __('Bijbehorende diensten', 'silverbee-starter'); ?>
                            </h3>
                            <div class="tax-diensten-wrapper">
                                <div class="row">
	                                <?php while ( have_posts() ) : the_post();
		                                $dienst_icon = get_the_post_thumbnail_url();
		                                $dienst_title = get_the_title();
		                                $dienst_desc = get_the_excerpt();
		                                $dienst_link = get_the_permalink();
		                                ?>

                                    <div class="col-12 col-sm-6 col-md-6 col-xl-4 margin dienst-card">
                                        <article class="tax-dienst fly-in">
                                            <div class="dienst-img">
                                                <img src="<?php echo $dienst_icon; ?>" alt="">
                                            </div>
                                            <div class="dienst-content">
                                                <div class="dienst-title">
                                                    <h4>
			                                            <?php echo $dienst_title; ?>
                                                    </h4>
                                                </div>
                                                <div class="dienst-desc">
		                                            <?php echo $dienst_desc; ?>
                                                </div>
                                            </div>
                                            <div class="dienst-footer">
                                                <div class="dienst-hfd">
                                                    <?php echo $tax_name; ?>
                                                </div>
                                                <div class="read-more-btn">
                                                    <a href="<?php echo $dienst_link; ?>" class="invert">
                                                        <strong>
	                                                        <?php echo __('Lees meer', 'silverbee-starter'); ?>
                                                        </strong>
                                                    </a>
                                                </div>
                                            </div>
                                        </article>
                                    </div>
	                                <?php endwhile; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>