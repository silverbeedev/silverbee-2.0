<?php
/**
 * The template for displaying the front-page
 *
 * @package Silverbee_Starter
 */


get_header(); ?>
    <article>
        <div id="front-page" class="page">
            <?php get_template_part('template-parts/front-page/content', 'slider') ?>
            <?php get_template_part('template-parts/front-page/content', 'cases') ?>
            <?php get_template_part('template-parts/front-page/content', 'diensten-intro') ?>
            <?php get_template_part('template-parts/content', 'contact-banner') ?>
            <?php get_template_part('template-parts/content', 'blog-archive') ?>
        </div>
    </article>
<?php
get_footer();
