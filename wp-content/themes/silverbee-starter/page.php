<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Silverbee_Starter
 */

get_header(); ?>
    <article>
        <section id="page" class="page default-page">
            <section id="section-1">
	            <?php
	            while ( have_posts() ) : the_post();
		            get_template_part( 'template-parts/content', 'page' );
	            endwhile; // End of the loop.
	            ?>
            </section>
            <section id="section-2">
                <div class="ocean-container">
                    <div class="ocean">
                        <div class="wave"></div>
                        <div class="wave"></div>
                    </div>
                </div>
            </section>
            <section id="section-3-pg">
                <?php get_template_part( 'template-parts/content', 'page-blokken' ); ?>
            </section>
	        <?php get_template_part('template-parts/content', 'vraag') ?>
	        <?php get_template_part('template-parts/content', 'contact-banner') ?>
	        <?php get_template_part('template-parts/content', 'blog-archive') ?>
        </section>
    </article>
<?php
get_footer();
