<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Silverbee_Starter
 */

get_header(); ?>
    <article>
        <section id="archive" class="archive-page blogpage">
	        <?php get_template_part( 'template-parts/content', 'archive-blog' ); ?>
			<?php get_template_part('template-parts/content', 'contact-banner') ?>
			<?php get_template_part('template-parts/content', 'vraag') ?>
        </section>
    </article>

<?php
get_footer();
