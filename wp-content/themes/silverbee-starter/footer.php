<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Silverbee_Starter
 */

if ( have_rows( 'contactinformatie', 'option' ) ):
	while ( have_rows( 'contactinformatie', 'option' ) ): the_row();
		$contact_stad     = get_sub_field( 'contact_stad' );
		$contact_adres    = get_sub_field( 'contact_adres' );
		$contact_postcode = get_sub_field( 'contact_postcode' );
		$contact_tel      = get_sub_field( 'contact_tel' );
		$contact_mail     = get_sub_field( 'contact_mail' );
	endwhile;
endif;

?>

</div><!-- #content -->
</main>

<footer>
    <div class="container-fluid">
        <div class="row justify-content-center h-100">
            <div class="col-6 col-md-6 col-xl-2 col-lg-3 align-self-center">
                <section class="site-branding">
                    <a href="/"><img src="/wp-content/themes/silverbee-starter/dist/img/logo.png" alt="logo"/></a>
                </section>
            </div>
            <div class="col-md-12 col-xl-4 col-lg-6 d-none d-lg-block">
                <div class="vestiging">
                    <p><strong><?php echo $contact_stad; ?></strong></p>
                    <p><?php echo $contact_adres; ?></p>
                    <p><?php echo $contact_postcode.' '.$contact_stad; ?></p>
                    <p>T: <?php echo $contact_tel; ?></p>
                </div>
            </div>
            <div class="col-6 col-md-6 col-xl-2 col-lg-3">
                <div class="contact-btn-container">
                    <?php
                        $contact_id = apply_filters( 'wpml_object_id', 123, 'post', true );
                        $contact_url = get_permalink($contact_id);
                    ?>
                    <a href="<?php echo $contact_url; ?>" class="btn-contact">
                        <?php echo __('Contact', 'silverbee-starter'); ?>
                    </a>
                </div>
                <div class="mail">
                    <a href="mailto:info@silverbee.nl"><span><?php get_template_part( 'img/mail-send.svg' ); ?></span>info@silverbee.nl</a>
                </div>
                <div class="social-media">
                    <div class="social">
                        <ul>
                            <?php
                            $in = get_field('option_linkedin', 'option');
                            $fb = get_field('option_facebook', 'option');
                            ?>
                            <li id="in">
                                <a href="<?php echo $in; ?>" target="_blank">
			                        <?php get_template_part( 'img/in.svg' ); ?>
                                </a>
                            </li>
                            <li id="fb">
                                <a href="<?php echo $fb; ?>" target="_blank">
			                        <?php get_template_part( 'img/fb.svg' ); ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
