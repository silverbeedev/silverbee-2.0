<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Silverbee_Starter
 */

get_header(); ?>
	<article>
		<section id="taxonomy" class="page taxonomy-page">
            <?php get_template_part( 'template-parts/content', 'taxonomy' ); ?>
            <?php get_template_part('template-parts/content', 'vraag') ?>
            <?php get_template_part('template-parts/content', 'related-case') ?>
            <?php get_template_part('template-parts/content', 'contact-banner') ?>
            <?php get_template_part('template-parts/content', 'blog-archive') ?>
		</section>
	</article>

<?php
get_footer();
