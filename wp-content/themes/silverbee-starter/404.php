<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package Silverbee_Starter
 */

get_header(); ?>
    <article>
        <section id="error-404" class="page">
            <div class="container-fluid">
                <div class="row justify-content-center">
                    <div class="col-sm-11 col-lg-10 col-xl-8">
                        <div class="row">
                            <div class="col-12 text-center text-sm-left">
                                <div class="error-header">
                                    <h1 class="page-title">
                                        <?php echo __('De pagina die je probeert de bereiken bestaat niet meer!', 'silverbee-starter'); ?>
                                    </h1>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-12 text-center col-sm-3 text-sm-left col-md-3 col-lg-2">
                                        <p class="e1">404</p>
                                        <p class="e2"><?php echo __('Pagina niet gevonden', 'silverbee-starter'); ?></p>
                                    </div>
                                    <div class="col-12 text-center col-sm-9 text-sm-left col-md-9 col-lg-10 align-self-center">
                                        <p class="e3">
                                            <?php echo __('Het kan zijn dat deze pagina is verwijderd, of er is een verkeerde url ingevoerd.
                                            Wij raden je aan om terug te gaan naar de homepagina.
                                            Als je op zoek bent naar een bepaald onderwerp kan je gebruik maken van onderstaande
                                            zoekbalk.', 'silverbee-starter'); ?>
                                        </p>
                                        <div class="btn-container">
                                            <a href="<?php echo get_home_url(); ?>" class="btn-primary">
                                                <?php echo __('Terug naar homepagina', 'silverbee-starter'); ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
	    <?php get_template_part('template-parts/content', 'contact-banner') ?>
    </article>
<?php
get_footer();
