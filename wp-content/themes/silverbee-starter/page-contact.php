<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 * Template Name: Page Contact
 * Template Post Type: page
 * @package Silverbee_Starter
 */

get_header();

if ( have_rows( 'contactinformatie', 'option' ) ):
	while ( have_rows( 'contactinformatie', 'option' ) ): the_row();
		$contact_stad     = get_sub_field( 'contact_stad' );
		$contact_adres    = get_sub_field( 'contact_adres' );
		$contact_postcode = get_sub_field( 'contact_postcode' );
		$contact_tel      = get_sub_field( 'contact_tel' );
		$contact_mail     = get_sub_field( 'contact_mail' );
	endwhile;
endif;

?>
    <article>
        <section id="page" class="page default-page page-contact">
            <div class="post-content post-content-contact">
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-11 col-md-6 col-lg-5 col-xl-4">
							<?php
							while ( have_posts() ) : the_post(); ?>
                                <h1>
									<?php echo get_the_title(); ?>
                                </h1>
								<?php the_content(); ?>
							<?php endwhile; // End of the loop.
							?>
                        </div>
                        <div class="col-11 col-md-5 col-lg-5 col-xl-4 align-self-center text-right">
                            <div id="map-container">
                                <div id="map"></div>
                                <svg id="overlay" pointer-events="none"></svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <section id="section-2" class="section-2-contact">
                <div class="ocean-container">
                    <div class="ocean">
                        <div class="wave"></div>
                        <div class="wave"></div>
                    </div>
                </div>
            </section>
            <section id="section-3-pg" class="section-3-contact">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12">
							<?php get_template_part( 'template-parts/content', 'page-contact' ); ?>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </article>
    <script>
        function initMap() {
            // Styles a map in night mode.
            var myLatLng = {lat: 51.339641, lng: 5.462887};

            var map = new google.maps.Map(document.getElementById('map'), {
                center: myLatLng,
                zoom: 13,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: false,
                rotateControl: false,
                fullscreenControl: false,
                styles: [
                    {
                        "featureType": "all",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "weight": "2.00"
                            }
                        ]
                    },
                    {
                        "featureType": "all",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#9c9c9c"
                            }
                        ]
                    },
                    {
                        "featureType": "all",
                        "elementType": "labels.text",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#899d93"
                            }
                        ]
                    },
                    {
                        "featureType": "all",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.land_parcel",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#899d93"
                            },
                            {
                                "saturation": "-25"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "all",
                        "stylers": [
                            {
                                "color": "#f2f2f2"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape.man_made",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "all",
                        "stylers": [
                            {
                                "saturation": -100
                            },
                            {
                                "lightness": 45
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#71c93e"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#7b7b7b"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#ceeabe"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#ceeabe"
                            }
                        ]
                    },
                    {
                        "featureType": "transit",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "all",
                        "stylers": [
                            {
                                "color": "#46bcec"
                            },
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#c8d7d4"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#899d93"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "color": "#8dd364"
                            },
                            {
                                "weight": "1.00"
                            }
                        ]
                    }
                ]
            });
            var image = '/wp-content/themes/silverbee-starter/dist/img/marker.png';
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                icon: image,
                title: 'Hello World!'
            });

            var stadnaam = '<?php echo $contact_stad; ?>';
            var adres = '<?php echo $contact_adres; ?>';
            var postcode = '<?php echo $contact_postcode; ?>';
            var routeText = '<?php echo __("Route", "silverbee-starter") ?>';

            var contentString = '<div id="info-window-content">' +
                '<div id="siteNotice">' + '</div>' +
                '<h2 id="firstHeading" class="firstHeading">Silverbee</h2>' +
                '<p>'+adres+',</p>' +
                '<p>'+postcode+' </br>'+stadnaam+'</p>' +
                '<p><div class="route"><a class="map-route" rel="nofollow" target="_blank" href="https://www.google.com/maps/dir/51.339537,5.4639617/Dragonder+8,+5554+GM+Valkenswaard/@51.339326,5.4613097,17z/data=!3m1!4b1!4m10!4m9!1m1!4e1!1m5!1m1!1s0x47c6d6f8be83fc2d:0x770725d7bf55e798!2m2!1d5.4628653!2d51.3393996!3e2">'+routeText+'</a> </div><p>' +
                '</div>';

            var infowindow = new google.maps.InfoWindow({
                content: contentString
            });

            google.maps.event.addListener(marker, 'click', function () {
                infowindow.open(map, marker);
            });


        }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDx0GsKcpZ2lIc8QdrDS7-LPxNQv6COifo&callback=initMap"
            async defer></script>
<?php
get_footer();
