<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Silverbee_Starter
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <!--    get analytics script-->
	<?php get_template_part( 'template-parts/analytics/content', 'head' ); ?>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!--    get analytics script-->
<?php get_template_part( 'template-parts/analytics/content', 'body' ); ?>
<div id="page" class="site">
    <header id="masthead" class="site-header" role="banner">
        <div id="primary-header">
            <div id="social-hex" class="hex-container">
                <div class="hex"></div>
                <ul class="social">
					<?php
					$in = get_field( 'option_linkedin', 'option' );
					$fb = get_field( 'option_facebook', 'option' );
					?>
                    <li id="in">
                        <a href="<?php echo $in; ?>" target="_blank">
							<?php get_template_part( 'img/in.svg' ); ?>
                        </a>
                    </li>
                    <li id="fb">
                        <a href="<?php echo $fb; ?>" target="_blank">
							<?php get_template_part( 'img/fb.svg' ); ?>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="navigation-header">
                <div class="container-fluid">
                    <div class="row justify-content-center">
                        <div class="col-xl-4 col-lg-5 col-11">
                            <section class="site-branding">
                                <a href="<?php echo get_home_url(); ?>"><?php get_template_part( 'img/sb-logo.svg' ); ?></a>
                            </section><!-- .site-branding -->
                        </div>
                        <div class="col-xl-4 col-lg-5">
                            <div class="switcher">
					            <?php do_action( 'wpml_add_language_selector' ); ?>
                            </div>
                            <nav class="site-navigation sub-navigation">
					            <?php
					            wp_nav_menu( array(
						            'theme_location' => 'menu-1',
						            'menu_id'        => 'sub-menu',
					            ) );
					            ?>
                            </nav><!-- #site-navigation -->

                        </div>
                    </div>
                    <div class="row justify-content-center">
                        <div class="col-xl-8 col-lg-10">
                            <nav class="site-navigation main-navigation">
					            <?php
					            wp_nav_menu( array(
						            'theme_location' => 'menu-2',
						            'menu_id'        => 'main-menu',
					            ) );
					            ?>
                                <div class="outer-menu">
                                    <label>
                                        <input id="menuTrigger" type="checkbox">

                                        <span class="menus">
                                        <span class="hamburger"></span>
                                    </span>
                                        <div class="overlay">
                                            <div class="container-fluid">
                                                <div class="row justify-content-center">
                                                    <div class="col-11 col-sm-5">
											            <?php
											            wp_nav_menu( array(
												            'theme_location' => 'menu-2',
												            'menu_id'        => 'main-menu',
											            ) );
											            ?>
											            <?php
											            wp_nav_menu( array(
												            'theme_location' => 'menu-1',
												            'menu_id'        => 'sub-menu',
											            ) );
											            ?>
                                                    </div>

										            <?php if ( have_rows( 'contactinformatie', 'option' ) ):
											            while ( have_rows( 'contactinformatie', 'option' ) ): the_row();
												            $contact_stad     = get_sub_field( 'contact_stad' );
												            $contact_adres    = get_sub_field( 'contact_adres' );
												            $contact_postcode = get_sub_field( 'contact_postcode' );
												            $contact_tel      = get_sub_field( 'contact_tel' );
												            $contact_mail     = get_sub_field( 'contact_mail' );
											            endwhile;
										            endif;
										            $in = get_field( 'option_linkedin', 'option' );
										            $fb = get_field( 'option_facebook', 'option' );
										            ?>

                                                    <div class="col-11 col-sm-6">
                                                        <div class="switcher overlay-switch">
                                                            <span><?php get_template_part( 'img/translation.svg' ); ?></span>
												            <?php do_action( 'wpml_add_language_selector' ); ?>
                                                        </div>
                                                        <div class="vestiging">
                                                            <i class="eindhoven"><?php get_template_part( 'img/lightbulb.svg' ); ?></i>
                                                            <p><strong><?php echo $contact_stad; ?></strong></p>
                                                            <p><?php echo $contact_adres; ?></p>
                                                            <p><?php echo $contact_postcode . ' ' . $contact_stad; ?></p>
                                                            <p>T: <?php echo $contact_tel; ?></p>
                                                        </div>
                                                        <div class="mail">
                                                            <a href="mailto:<?php echo $contact_mail; ?>">
                                                                <span><?php get_template_part( 'img/mail-send.svg' ); ?></span>
													            <?php echo $contact_mail; ?>
                                                            </a>
                                                        </div>
                                                        <div class="social-media">
                                                            <div class="social">
                                                                <ul>
                                                                    <li id="in">
                                                                        <a href="<?php echo $in; ?>" target="_blank">
																            <?php get_template_part( 'img/in.svg' ); ?>
                                                                        </a>
                                                                    </li>
                                                                    <li id="fb">
                                                                        <a href="<?php echo $fb; ?>" target="_blank">
																            <?php get_template_part( 'img/fb.svg' ); ?>
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </label>
                                </div>
                            </nav><!-- #site-navigation -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header><!-- #masthead -->

    <main>
        <div id="content" class="site-content">

