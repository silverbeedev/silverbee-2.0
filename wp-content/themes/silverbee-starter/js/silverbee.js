(function ($) {

    $(document).ready(function(){

        function equalHeight(group) {
            var tallest = 0;
            group.each(function() {
                var thisHeight = $(this).height();
                if(thisHeight > tallest) {
                    tallest = thisHeight;
                }
            });
            group.height(tallest);
        }
        equalHeight($(".dienst-content"));
        equalHeight($(".dienst-img"));

        $('.menus').click(function(){
            $('body').toggleClass('disable-overflow');
            $('#page').toggleClass('disable-overflow');
        });

        $(window).scroll(function () {
            if ($(this).scrollTop() >= 400 ) { // this refers to window
                $('header').addClass('is-sticky');
            }
            else {
                $('header').removeClass('is-sticky');
            }
        });

    });

})(jQuery);
window.onload = function () {

        // Get the checkbox
    document.getElementById('menuTrigger').onclick = function(){
        document.getElementsByClassName('front-cta-container')[0].classList.toggle("menu-open");
    }

    // Define our viewportWidth variable
    var viewportWidth;

// Set/update the viewportWidth value
    var setViewportWidth = function () {
        viewportWidth = window.innerWidth || document.documentElement.clientWidth;
    }

// Log the viewport width into the console
    var logWidth = function () {
        if (viewportWidth > 768) {

            (function ($) {
                $(window).scroll(function () {
                    $("#section-1 .image-container").css("opacity", 1 - $(window).scrollTop() / 550);
                    $(".front-cta-container").css("opacity", 1 - $(window).scrollTop() / 550);
                    $("#social-hex").css("opacity", 1 - $(window).scrollTop() / 550);
                });
            })(jQuery);

            (function ($) {
                //move homepage image
                var
                    lFollowX2 = 0,
                    lFollowY2 = 0,
                    x2 = 0,
                    y2 = 0,

                    friction = 1 / 30;

                function moveBackground() {
                    x2 += (lFollowX2 - x2) * friction;
                    y2 += (lFollowY2 - y2) * friction;
                    translate2 = x2 + 'px ' + y2 + 'px';
                    $('.image').css({
                        'background-position': translate2,
                    });
                    window.requestAnimationFrame(moveBackground);
                }
                $(window).on('mousemove click', function (e) {
                    var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
                    var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
                    lFollowX2 = -0 + (20 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
                    lFollowY2 = 0 + (10 * lMouseY) / 100;
                });
                moveBackground();

            })(jQuery);
        }
    }

// Set our initial width and log it
    setViewportWidth();
    logWidth();

// On resize events, recalculate and log
    window.addEventListener('resize', function () {
        setViewportWidth();
        logWidth();
    }, false);



    const sections = document.querySelectorAll('.fly-in');

    window.onscroll = function() {
        // Don't run the rest of the code if every section is already visible
        if (!document.querySelectorAll('.fly-in:not(.visible)')) return;

        // Run this code for every section in sections
        for (const section of sections) {
            if (section.getBoundingClientRect().top <= window.innerHeight * 0.75 && section.getBoundingClientRect().top > 0) {
                section.classList.add('visible');
            }
        }
    };




    function resizeGridItem(item){
        grid = document.getElementsByClassName("masonry-row")[0];
        rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'));
        rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'));
        rowSpan = Math.ceil((item.querySelector('.blog-post').getBoundingClientRect().height+rowGap)/(rowHeight+rowGap));
        item.style.gridRowEnd = "span "+rowSpan;
    }

    function resizeAllGridItems(){
        allItems = document.getElementsByClassName("blog-post-container");
        for(x=0;x<allItems.length;x++){
            resizeGridItem(allItems[x]);
        }
    }

    function resizeInstance(instance){
        item = instance.elements[0];
        resizeGridItem(item);
    }

    window.onload = resizeAllGridItems();
    window.addEventListener("resize", resizeAllGridItems);

    // allItems = document.getElementsByClassName("blog-post-container");
    // for(x=0;x<allItems.length;x++){
    //     imagesLoaded( allItems[x], resizeInstance);
    // }
}

