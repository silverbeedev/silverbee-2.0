<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Silverbee_Starter
 */

get_header(); ?>
    <article>
        <section id="archive" class="archive-page">
			<?php
			if ( is_post_type_archive( 'cases' ) ) {
				get_template_part( 'template-parts/content', 'archive-cases' );
			} elseif ( is_post_type_archive( 'teamleden' ) ) {
				get_template_part( 'template-parts/content', 'archive-teamleden' );
			} elseif ( is_post_type_archive( 'diensten' ) ) {
				get_template_part( 'template-parts/content', 'archive-diensten' );
			}
			?>
			<?php get_template_part( 'template-parts/content', 'contact-banner' ) ?>
			<?php
			if ( ! is_post_type_archive( 'teamleden' ) ) {
				get_template_part( 'template-parts/content', 'vraag' );
            } ?>
			<?php get_template_part( 'template-parts/content', 'blog-archive' ) ?>
        </section>
    </article>

<?php
get_footer();
