<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 12/11/2019
 * Time: 10:35
 * Template Name: Page Hoofddiensten
 * Template Post Type: page
 */

get_header(); ?>
	<article>
		<section id="archive" class="archive-page">

			<section id="section-1">
				<div class="post-content">
					<div class="container-fluid">
						<div class="row justify-content-center">
							<div class="col-11 col-md-11 col-lg-10 col-xl-8 align-self-center">
								<div class="cases-intro-container">
									<h1>
										<?php echo get_the_title(); ?>
									</h1>
									<?php the_content(); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>

			<section id="section-2">
				<div class="ocean-container">
					<div class="ocean">
						<div class="wave"></div>
						<div class="wave"></div>
					</div>
				</div>
				<div class="archive-diensten">
					<?php get_template_part( 'template-parts/front-page/content', 'diensten-intro' ) ?>
				</div>
			</section>

			<?php get_template_part('template-parts/content', 'contact-banner') ?>
			<?php get_template_part('template-parts/content', 'vraag') ?>
			<?php get_template_part('template-parts/content', 'blog-archive') ?>
		</section>
	</article>

<?php
get_footer();


