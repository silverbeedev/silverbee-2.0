<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Silverbee_Starter
 */

get_header(); ?>
    <article>
        <section id="single" class="page single-page">
			<?php if ( is_singular( 'diensten' ) ) {
				get_template_part( 'template-parts/content', 'single-dienst' );
			} elseif ( is_singular( 'post' ) ) {
				get_template_part( 'template-parts/content', 'single-blog' );
			} elseif ( is_singular( 'teamleden' ) ) {
				get_template_part( 'template-parts/content', 'single-teamlid' );
			}
			else {
				get_template_part( 'template-parts/content', 'single-case' );
			}

			if ( ! is_singular( 'cases' && ! is_singular( 'post' ) ) ) {
				get_template_part( 'template-parts/content', 'related-case' );
			}

			if ( ! is_singular( 'teamleden' ) ) {
				get_template_part( 'template-parts/content', 'vraag' );
			}
			get_template_part( 'template-parts/content', 'contact-banner' );

			if ( ! is_singular( 'post' ) ) {
				get_template_part( 'template-parts/content', 'blog-archive' );
			}
			?>
        </section>
    </article>

<?php
get_footer();
