<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
     viewBox="0 0 377.5 377.5" enable-background="new 0 0 377.5 377.5" xml:space="preserve">
<g>
    <defs>
        <path id="SVGID_1_" fill="red" d="M1.3,164.9l37,138.2c4.2,15.7,20.3,31.8,36,36l138.2,37.1c15.6,4.2,37.7-1.7,49.2-13.2L363,261.8
			c11.5-11.5,17.4-33.5,13.2-49.2l-37-138.2c-4.2-15.7-20.3-31.8-36-36L164.9,1.4c-15.6-4.2-37.7,1.7-49.2,13.2L14.5,115.7
			C3.1,127.2-2.8,149.2,1.3,164.9z"/>
    </defs>


</g>
</svg>
