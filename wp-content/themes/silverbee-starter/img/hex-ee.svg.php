<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 377.5 377.5" enable-background="new 0 0 377.5 377.5" xml:space="preserve">
<g>
	<defs>
		<path id="ee" d="M212.6,376.2L74.4,339.1c-15.6-4.2-31.8-20.3-36-36l-37-138.2c-4.2-15.7,1.7-37.7,13.2-49.2L115.7,14.5
			c11.5-11.5,33.5-17.4,49.2-13.2l138.2,37.1c15.6,4.2,31.8,20.3,36,36l37,138.2c4.2,15.7-1.7,37.7-13.2,49.2L261.8,363
			C250.3,374.5,228.2,380.4,212.6,376.2z"/>
	</defs>
	<clipPath id="ee_1_">
		<use xlink:href="#ee"  overflow="visible"/>
	</clipPath>
	<g id="YVZwdp_1_" clip-path="url(#ee_1_)">
		
			<image overflow="visible" width="600" height="600" id="YVZwdp_2_" xlink:href="77DFEEB319D4D8A3.jpg"  transform="matrix(0.6292 0 0 0.6292 0 1.149074e-02)">
		</image>
	</g>
</g>
</svg>
