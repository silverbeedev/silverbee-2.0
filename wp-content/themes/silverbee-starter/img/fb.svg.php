<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 39.4 39.401">
  <defs>
    <style>
      .cls-1, .cls-4 {
        fill: none;
      }

      .cls-1 {
        stroke: #fff;
        stroke-width: 2px;
        stroke-dasharray: 5;
        opacity: 0.397;
      }

      .cls-2 {
        fill: #fff;
      }

      .cls-3 {
        stroke: none;
      }
    </style>
  </defs>
  <g id="fb" transform="translate(-33.599 -253.086)">
    <g id="twitter" transform="translate(33.599 253.086)">
      <g id="Ellipse_8" data-name="Ellipse 8" class="cls-1">
        <ellipse class="cls-3" cx="19.7" cy="19.7" rx="19.7" ry="19.7"/>
        <ellipse class="cls-4" cx="19.7" cy="19.7" rx="18.7" ry="18.7"/>
      </g>
    </g>
    <g id="facebook-letter-logo" transform="translate(-66.696 262.263)">
      <path id="Facebook" class="cls-2" d="M116.9,4.232V7.24h-2.2v3.678h2.2v10.93h4.527V10.919h3.038s.285-1.764.422-3.692h-3.443V4.712a1.068,1.068,0,0,1,.981-.882h2.466V0h-3.354C116.788,0,116.9,3.683,116.9,4.232Z"/>
    </g>
  </g>
</svg>
