<?php

function cases_init() {
	$labels = array(
		'name'               => _x( 'Cases', 'post type general name', 'silverbee-core' ),
		'singular_name'      => _x( 'Case', 'post type singular name', 'silverbee-core' ),
		'menu_name'          => _x( 'Cases', 'admin menu', 'silverbee-core' ),
		'name_admin_bar'     => _x( 'Cases', 'add new on admin bar', 'silverbee-core' ),
		'add_new'            => _x( 'Nieuwe toevoegen', 'silverbee-core' ),
		'add_new_item'       => __( 'Nieuwe case', 'silverbee-core' ),
		'new_item'           => __( 'Nieuwe case', 'silverbee-core' ),
		'edit_item'          => __( 'Edit case', 'silverbee-core' ),
		'view_item'          => __( 'Bekijk case', 'silverbee-core' ),
		'all_items'          => __( 'Alle cases', 'silverbee-core' ),
		'search_items'       => __( 'Zoek cases:', 'silverbee-core' ),
		'not_found'          => __( 'Geen cases gevonden', 'silverbee-core' ),
		'not_found_in_trash' => __( 'Geen cases gevonden in de prullenbak', 'silverbee-core' )
	);
	$args   = array(
		'labels'             => $labels,
		'description'        => __( 'Description', 'silverbee-core' ),
		'menu_icon'           => 'dashicons-portfolio',
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'menu_position'         => 3,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'cases' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'supports'           => array( 'title', 'editor', 'excerpt', 'comments', 'thumbnail','page-attributes'),
		'taxonomies'         => false
	);
	register_post_type( 'cases', $args );
	require_once( dirname( __FILE__ ) . '/../acf/cases.php' );
}