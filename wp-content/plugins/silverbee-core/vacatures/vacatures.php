<?php

function vacatures_init() {
	$labels = array(
		'name'               => _x( 'vacatures', 'post type general name', 'silverbee-core' ),
		'singular_name'      => _x( 'Vacature', 'post type singular name', 'silverbee-core' ),
		'menu_name'          => _x( 'Vacatures', 'admin menu', 'silverbee-core' ),
		'name_admin_bar'     => _x( 'Vacatures', 'add new on admin bar', 'silverbee-core' ),
		'add_new'            => _x( 'Nieuwe toevoegen', 'silverbee-core' ),
		'add_new_item'       => __( 'Nieuwe vacature', 'silverbee-core' ),
		'new_item'           => __( 'Nieuwe vacature', 'silverbee-core' ),
		'edit_item'          => __( 'Edit vacature', 'silverbee-core' ),
		'view_item'          => __( 'Bekijk vacature', 'silverbee-core' ),
		'all_items'          => __( 'Alle vacatures', 'silverbee-core' ),
		'search_items'       => __( 'Zoek vacatures:', 'silverbee-core' ),
		'not_found'          => __( 'Geen vacatures gevonden', 'silverbee-core' ),
		'not_found_in_trash' => __( 'Geen vacatures gevonden in de prullenbak', 'silverbee-core' )
	);
	$args   = array(
		'labels'             => $labels,
		'description'        => __( 'Description', 'silverbee-core' ),
		'menu_icon'           => 'dashicons-id-alt',
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'menu_position'         => 2,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'vacatures' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'supports'           => array( 'title', 'editor', 'excerpt', 'comments', 'thumbnail','page-attributes'),
		'taxonomies'         => false
	);
	register_post_type( 'vacatures', $args );
	require_once( dirname( __FILE__ ) . '/../acf/vacatures.php' );
}