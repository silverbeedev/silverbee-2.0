<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 10/01/2019
 * Time: 10:04
 */

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5c37215c769c8',
		'title' => 'Extra velden "Aanbevelingen"',
		'fields' => array(
			array(
				'key' => 'field_5c37216aaa22b',
				'label' => 'Functie',
				'name' => 'aanbeveling_functie',
				'type' => 'text',
				'instructions' => 'Voeg hier de functie of het bedrijf van de betreffende persoon toe.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
			array(
				'key' => 'field_5c37218daa22c',
				'label' => 'Gekoppelde diensten',
				'name' => 'aanbeveling_gekoppelde_diensten',
				'type' => 'taxonomy',
				'instructions' => 'Koppel hier de diensten die bij deze aanbeveling getoond worden.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'taxonomy' => 'hoofddiensten',
				'field_type' => 'checkbox',
				'add_term' => 1,
				'save_terms' => 0,
				'load_terms' => 0,
				'return_format' => 'id',
				'multiple' => 0,
				'allow_null' => 0,
			),
			array(
				'key' => 'field_5c3721beaa22d',
				'label' => 'Gekoppelde cases',
				'name' => 'aanbeveling_gekoppelde_cases',
				'type' => 'relationship',
				'instructions' => 'Koppel hier de cases die bij deze aanbeveling getoond worden.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'post_type' => array(
					0 => 'cases',
				),
				'taxonomy' => '',
				'filters' => array(
					0 => 'search',
					1 => 'post_type',
					2 => 'taxonomy',
				),
				'elements' => '',
				'min' => '',
				'max' => '',
				'return_format' => 'id',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'aanbevelingen',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));

endif;