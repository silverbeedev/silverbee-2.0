<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 10/01/2019
 * Time: 10:13
 */

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5c3723919b4e1',
		'title' => 'Extra velden "Vacatures"',
		'fields' => array(
			array(
				'key' => 'field_5c372e1a627e1',
				'label' => 'Functie',
				'name' => 'vacature_functie',
				'type' => 'text',
				'instructions' => 'Voer hier de functie van deze vacature in.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'placeholder' => '',
				'prepend' => '',
				'append' => '',
				'maxlength' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'vacatures',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));

endif;