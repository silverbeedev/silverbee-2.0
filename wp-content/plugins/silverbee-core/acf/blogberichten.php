<?php
/**
 * Created by PhpStorm.
 * User: rubenverschuren
 * Date: 10/01/2019
 * Time: 13:42
 */

if( function_exists('acf_add_local_field_group') ):

	acf_add_local_field_group(array(
		'key' => 'group_5c3735b386386',
		'title' => 'Extra velden "Blogberichten"',
		'fields' => array(
			array(
				'key' => 'field_5c3735c1b3c6d',
				'label' => 'Inleiding',
				'name' => 'blog_inleiding',
				'type' => 'wysiwyg',
				'instructions' => 'Voer hier de inleiding van dit blogbericht in.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'default_value' => '',
				'tabs' => 'all',
				'toolbar' => 'full',
				'media_upload' => 1,
				'delay' => 0,
			),
			array(
				'key' => 'field_5c373855b3c6e',
				'label' => 'Gekoppeld teamlid',
				'name' => 'blog_gekoppeld_teamlid',
				'type' => 'relationship',
				'instructions' => 'Koppel hier het teamlid dat bij dit blogbericht getoond wordt.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'post_type' => array(
					0 => 'teamleden',
				),
				'taxonomy' => '',
				'filters' => array(
					0 => 'search',
					1 => 'post_type',
					2 => 'taxonomy',
				),
				'elements' => '',
				'min' => '',
				'max' => '1',
				'return_format' => 'id',
			),
			array(
				'key' => 'field_5c373d0ab3c6f',
				'label' => 'Gekoppelde diensten',
				'name' => 'blog_gekoppelde_diensten',
				'type' => 'taxonomy',
				'instructions' => 'Koppel hier de diensten die bij dit blogbericht getoond worden.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'taxonomy' => 'hoofddiensten',
				'field_type' => 'checkbox',
				'add_term' => 1,
				'save_terms' => 0,
				'load_terms' => 0,
				'return_format' => 'id',
				'multiple' => 0,
				'allow_null' => 0,
			),
			array(
				'key' => 'field_5c373d4fb3c70',
				'label' => 'Gekoppelde cases',
				'name' => 'blog_gekoppelde_cases',
				'type' => 'relationship',
				'instructions' => 'Koppel hier de cases die bij dit blogbericht getoond worden.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'post_type' => array(
					0 => 'cases',
				),
				'taxonomy' => '',
				'filters' => array(
					0 => 'search',
					1 => 'post_type',
					2 => 'taxonomy',
				),
				'elements' => '',
				'min' => '',
				'max' => '',
				'return_format' => 'id',
			),
			array(
				'key' => 'field_5c373d80b3c71',
				'label' => 'Gallerij',
				'name' => 'blog_gallerij',
				'type' => 'gallery',
				'instructions' => 'Voeg in deze gallerij afbeeldingen toe met betrekking tot deze case.',
				'required' => 0,
				'conditional_logic' => 0,
				'wrapper' => array(
					'width' => '',
					'class' => '',
					'id' => '',
				),
				'min' => '',
				'max' => '',
				'insert' => 'append',
				'library' => 'all',
				'min_width' => '',
				'min_height' => '',
				'min_size' => '',
				'max_width' => '',
				'max_height' => '',
				'max_size' => '',
				'mime_types' => '',
			),
		),
		'location' => array(
			array(
				array(
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'post',
				),
			),
		),
		'menu_order' => 0,
		'position' => 'normal',
		'style' => 'default',
		'label_placement' => 'top',
		'instruction_placement' => 'label',
		'hide_on_screen' => '',
		'active' => 1,
		'description' => '',
	));

endif;
