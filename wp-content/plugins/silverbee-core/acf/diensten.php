<?php
///**
// * Created by PhpStorm.
// * User: rubenverschuren
// * Date: 09/01/2019
// * Time: 18:06
// */
//
//if( function_exists('acf_add_local_field_group') ):
//
//	acf_add_local_field_group(array(
//		'key' => 'group_5c3716f7a84d8',
//		'title' => 'Extra velden "Diensten"',
//		'fields' => array(
//			array(
//				'key' => 'field_5c3716fe5366e',
//				'label' => 'Inleiding',
//				'name' => 'dienst_inleiding',
//				'type' => 'wysiwyg',
//				'instructions' => 'Voer hier de inleiding van deze dienst in.',
//				'required' => 0,
//				'conditional_logic' => 0,
//				'wrapper' => array(
//					'width' => '',
//					'class' => '',
//					'id' => '',
//				),
//				'default_value' => '',
//				'tabs' => 'all',
//				'toolbar' => 'full',
//				'media_upload' => 1,
//				'delay' => 0,
//			),
//			array(
//				'key' => 'field_5c37177a5366f',
//				'label' => 'Gekoppeld teamlid',
//				'name' => 'dienst_gekoppeld_teamlid',
//				'type' => 'relationship',
//				'instructions' => 'Koppel hier het teamlid dat bij deze dienst getoond wordt.',
//				'required' => 0,
//				'conditional_logic' => 0,
//				'wrapper' => array(
//					'width' => '',
//					'class' => '',
//					'id' => '',
//				),
//				'post_type' => array(
//					0 => 'teamleden',
//				),
//				'taxonomy' => '',
//				'filters' => array(
//					0 => 'search',
//					1 => 'post_type',
//					2 => 'taxonomy',
//				),
//				'elements' => '',
//				'min' => '',
//				'max' => 1,
//				'return_format' => 'id',
//			),
//			array(
//				'key' => 'field_5c371a9453670',
//				'label' => 'Gekoppelde cases',
//				'name' => 'dienst_gekoppelde_cases',
//				'type' => 'relationship',
//				'instructions' => 'Koppel hier de cases die bij deze dienst getoond worden.',
//				'required' => 0,
//				'conditional_logic' => 0,
//				'wrapper' => array(
//					'width' => '',
//					'class' => '',
//					'id' => '',
//				),
//				'post_type' => array(
//					0 => 'cases',
//				),
//				'taxonomy' => '',
//				'filters' => array(
//					0 => 'search',
//					1 => 'post_type',
//					2 => 'taxonomy',
//				),
//				'elements' => '',
//				'min' => '',
//				'max' => '',
//				'return_format' => 'id',
//			),
//		),
//		'location' => array(
//			array(
//				array(
//					'param' => 'post_type',
//					'operator' => '==',
//					'value' => 'diensten',
//				),
//			),
//		),
//		'menu_order' => 0,
//		'position' => 'normal',
//		'style' => 'default',
//		'label_placement' => 'top',
//		'instruction_placement' => 'label',
//		'hide_on_screen' => '',
//		'active' => 1,
//		'description' => '',
//	));
//
//endif;