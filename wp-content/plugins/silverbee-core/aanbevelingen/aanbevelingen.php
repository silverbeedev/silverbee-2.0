<?php

function aanbevelingen_init() {
	$labels = array(
		'name'               => _x( 'Aanbevelingen', 'post type general name', 'silverbee-core' ),
		'singular_name'      => _x( 'Aanbeveling', 'post type singular name', 'silverbee-core' ),
		'menu_name'          => _x( 'Aanbevelingen', 'admin menu', 'silverbee-core' ),
		'name_admin_bar'     => _x( 'Aanbevelingen', 'add new on admin bar', 'silverbee-core' ),
		'add_new'            => _x( 'Nieuwe toevoegen', 'silverbee-core' ),
		'add_new_item'       => __( 'Nieuwe aanbeveling', 'silverbee-core' ),
		'new_item'           => __( 'Nieuwe aanbeveling', 'silverbee-core' ),
		'edit_item'          => __( 'Edit aanbeveling', 'silverbee-core' ),
		'view_item'          => __( 'Bekijk aanbeveling', 'silverbee-core' ),
		'all_items'          => __( 'Alle aanbevelingen', 'silverbee-core' ),
		'search_items'       => __( 'Zoek aanbevelingen:', 'silverbee-core' ),
		'not_found'          => __( 'Geen aanbevelingen gevonden', 'silverbee-core' ),
		'not_found_in_trash' => __( 'Geen aanbevelingen gevonden in de prullenbak', 'silverbee-core' )
	);
	$args   = array(
		'labels'             => $labels,
		'description'        => __( 'Description', 'silverbee-core' ),
		'menu_icon'           => 'dashicons-thumbs-up',
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'menu_position'         => 2,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'aanbevelingen' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'supports'           => array( 'title', 'editor', 'excerpt', 'comments', 'thumbnail','page-attributes'),
		'taxonomies'         => false
	);
	register_post_type( 'aanbevelingen', $args );
	require_once( dirname( __FILE__ ) . '/../acf/aanbevelingen.php' );
}