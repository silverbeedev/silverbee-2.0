<?php

/*
Plugin Name: Silverbee Core
Plugin URI: http://silverbee.nl
Description: Core plugin voor Silverbee. Deze plugin bevat alle op maat gemaakte CPT voor de website van Silverbee.
Version: 1.0
Author: Silverbee V.O.F
Author URI: http://silverbee.nl
License: GPL2
*/

require_once( 'aanbevelingen/aanbevelingen.php' );
require_once( 'diensten/diensten.php' );
require_once( 'cases/cases.php' );
require_once ( 'teamleden/teamleden.php' );
require_once ( 'vacatures/vacatures.php' );

function silverbee_init() {

	diensten_init();
    hoofddiensten_init();
    aanbevelingen_init();
	cases_init();
	teamleden_init();
	vacatures_init();

	require_once( dirname( __FILE__ ) . '/acf/divers.php' );
//	require_once( dirname( __FILE__ ) . '/acf/blogberichten.php' );
//	require_once( dirname( __FILE__ ) . '/acf/pages.php' );
}

add_action( 'init', 'silverbee_init' );

add_action( 'init', 'cp_change_post_object' );
// Change dashboard Posts to Blogberichten
function cp_change_post_object() {
	$get_post_type = get_post_type_object('post');
	$labels = $get_post_type->labels;
	$labels->name = 'Blogberichten';
	$labels->singular_name = 'blogberichten';
	$labels->add_new = 'Nieuwe toevoegen';
	$labels->add_new_item = 'Nieuwe toevoegen';
	$labels->edit_item = 'Edit blogberichten';
	$labels->new_item = 'blogberichten';
	$labels->view_item = 'Bekijk blogberichten';
	$labels->search_items = 'Zoek blogberichten';
	$labels->not_found = 'Geen blogberichten gevonden';
	$labels->not_found_in_trash = 'Geen blogberichten gevonden in de prullenbak';
	$labels->all_items = 'Alle blogberichten';
	$labels->menu_name = 'Blogberichten';
	$labels->name_admin_bar = 'Blogberichten';
}

if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Silverbee',
		'menu_title'	=> 'Silverbee',
		'menu_slug' 	=> 'silverbee-options',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

}