<?php

function teamleden_init() {
	$labels = array(
		'name'               => _x( 'Teamleden', 'post type general name', 'silverbee-core' ),
		'singular_name'      => _x( 'Teamlid', 'post type singular name', 'silverbee-core' ),
		'menu_name'          => _x( 'Teamleden', 'admin menu', 'silverbee-core' ),
		'name_admin_bar'     => _x( 'Teamleden', 'add new on admin bar', 'silverbee-core' ),
		'add_new'            => _x( 'Nieuwe toevoegen', 'silverbee-core' ),
		'add_new_item'       => __( 'Nieuwe teamlid', 'silverbee-core' ),
		'new_item'           => __( 'Nieuwe teamlid', 'silverbee-core' ),
		'edit_item'          => __( 'Edit teamlid', 'silverbee-core' ),
		'view_item'          => __( 'Bekijk teamlid', 'silverbee-core' ),
		'all_items'          => __( 'Alle teamleden', 'silverbee-core' ),
		'search_items'       => __( 'Zoek teamleden:', 'silverbee-core' ),
		'not_found'          => __( 'Geen teamleden gevonden', 'silverbee-core' ),
		'not_found_in_trash' => __( 'Geen teamleden gevonden in de prullenbak', 'silverbee-core' )
	);
	$args   = array(
		'labels'             => $labels,
		'description'        => __( 'Description', 'silverbee-core' ),
		'menu_icon'           => 'dashicons-groups',
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'menu_position'         => 2,
		'show_in_menu'       => true,
		'query_var'          => true,
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'supports'           => array( 'title', 'editor', 'excerpt', 'comments', 'thumbnail','page-attributes'),
		'taxonomies'         => false
	);
	register_post_type( 'teamleden', $args );
	require_once( dirname( __FILE__ ) . '/../acf/teamleden.php' );
}