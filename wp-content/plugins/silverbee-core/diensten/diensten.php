<?php

function diensten_init() {
	$labels = array(
		'name'               => _x( 'Diensten', 'post type general name', 'silverbee-core' ),
		'singular_name'      => _x( 'Dienst', 'post type singular name', 'silverbee-core' ),
		'menu_name'          => _x( 'Diensten', 'admin menu', 'silverbee-core' ),
		'name_admin_bar'     => _x( 'Diensten', 'add new on admin bar', 'silverbee-core' ),
		'add_new'            => _x( 'Nieuwe toevoegen', 'silverbee-core' ),
		'add_new_item'       => __( 'Nieuwe dienst', 'silverbee-core' ),
		'new_item'           => __( 'Nieuwe dienst', 'silverbee-core' ),
		'edit_item'          => __( 'Edit dienst', 'silverbee-core' ),
		'view_item'          => __( 'Bekijk dienst', 'silverbee-core' ),
		'all_items'          => __( 'Alle diensten', 'silverbee-core' ),
		'search_items'       => __( 'Zoek diensten:', 'silverbee-core' ),
		'not_found'          => __( 'Geen diensten gevonden', 'silverbee-core' ),
		'not_found_in_trash' => __( 'Geen diensten gevonden in de prullenbak', 'silverbee-core' )
	);
	$args   = array(
		'labels'             => $labels,
		'description'        => __( 'Description', 'silverbee-core' ),
		'menu_icon'           => 'dashicons-hammer',
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'menu_position'         => 4,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'dienst' ),
		'capability_type'    => 'post',
		'has_archive'        => false,
		'hierarchical'       => false,
		'supports'           => array( 'title', 'editor', 'excerpt', 'comments', 'thumbnail','page-attributes'),
		'taxonomies'         => false
	);
	register_post_type( 'diensten', $args );
	require_once( dirname( __FILE__ ) . '/../acf/diensten.php' );
}


function hoofddiensten_init() {
	$labels1 = array(
		'name'              => _x( 'Hoofddiensten', 'taxonomy general name', 'silverbee-core' ),
		'singular_name'     => _x( 'Hoofddiensten', 'taxonomy singular name', 'silverbee-core' ),
		'search_items'      => __( 'Zoek Hoofddiensten', 'silverbee-core' ),
		'all_items'         => __( 'Alle Hoofddiensten', 'silverbee-core' ),
		'parent_item'       => __( 'Parent Hoofddiensten', 'silverbee-core' ),
		'parent_item_colon' => __( 'Parent Hoofddiensten:', 'silverbee-core' ),
		'edit_item'         => __( 'Edit Hoofddienst', 'silverbee-core' ),
		'update_item'       => __( 'Update Hoofddienst', 'silverbee-core' ),
		'add_new_item'      => __( 'Nieuwe Hoofddienst', 'silverbee-core' ),
		'new_item_name'     => __( 'Nieuw Hoofddiensten', 'silverbee-core' ),
		'menu_name'         => __( 'Hoofddiensten', 'silverbee-core' ),
	);

	$args1 = array(
		'hierarchical'      => true,
		'labels'            => $labels1,
		'public'            => true,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'show_in_nav_menus' => true,
		'rewrite'            => array( 'slug' => 'diensten' ),
	);
	register_taxonomy( 'hoofddiensten', array( 'diensten' ), $args1 );
}